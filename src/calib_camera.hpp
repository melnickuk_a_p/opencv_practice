#pragma once
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <vector>

inline void calibrate_cam();

class calib_camera {
private:
	std::vector<cv::Point3f> objp;
	std::vector<std::string> images_str;
	cv::Size board_size;
	cv::Mat A, dist_koeff;
	float length;

public:
	calib_camera() = default;

	calib_camera(const std::string& str_path, const cv::Size& board, float size_square):board_size(board), length(size_square) {
		cv::glob(str_path, images_str);

		for (int i{}; i < board_size.height; ++i)
			for (int j{}; j < board_size.width; ++j)
				objp.push_back(cv::Point3f(j * length, i * length, 0));
	};

	void calibrate(cv::InputOutputArray K, cv::InputOutputArray distK) {
		std::vector<std::vector<cv::Point3f>> objpoints;
		std::vector<std::vector<cv::Point2f>> img_points;
		std::vector<cv::Point2f> corner_pts;

		cv::Mat frame, gray;

		for (int i{}; i < images_str.size(); ++i) {
			frame = cv::imread(images_str.at(i));
			cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);

			bool success = cv::findChessboardCorners(gray, board_size, corner_pts);

			if (success) {
				cv::TermCriteria criteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 100, 1e-5);
				cv::Size winSize = cv::Size(5, 5);
				cv::Size zeroZone = cv::Size(-1, -1);

				cv::cornerSubPix(gray, corner_pts, winSize, zeroZone, criteria);
				
				cv::drawChessboardCorners(frame, board_size, corner_pts, success);

				objpoints.push_back(objp);
				img_points.push_back(corner_pts);
			}

			cv::namedWindow("Image",0);
			cv::imshow("Image", frame);
			cv::waitKey(100);
		}

		cv::destroyAllWindows();

		cv::Mat R, t;
		cv::calibrateCamera(objpoints, img_points, frame.size(), K, distK, R, t);

		A = K.getMat();
		dist_koeff = distK.getMat();
	}


	void save_calib(const std::string& filename) {
		cv::FileStorage fs(filename, cv::FileStorage::WRITE);
		
		if (!fs.isOpened()) {
			std::cerr << "Can't open file: " << filename << std::endl;
			return;
		}
		
		fs << "Size board" << board_size;
		fs << "Square size" << length;

		fs << "camera_matrix" << A;
		fs << "distortion_coefficients" << dist_koeff;

		fs.release();
	}
};

inline void calibrate_cam() {
	calib_camera cam_l("../pictures/calib/left/auditory", cv::Size(9, 6), 0.024);

	cv::Mat cam_mat, dist_koeff;
	cam_l.calibrate(cam_mat, dist_koeff);

	cam_l.save_calib("../config/cam_artem.yaml");
}