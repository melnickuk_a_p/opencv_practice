#pragma once
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <vector>

inline void calibrate_stereo();

class calib_stereo {
private:
	std::vector<std::string> imgL_str;
	std::vector<std::string> imgR_str;

	std::vector<cv::Point3f> objp;

	cv::Size board_size;
	float length;

	cv::Mat Rotation, translation, Essential, Fundamental;
	cv::Mat camL_mat, camR_mat, distL_mat, distR_mat;
public:
	calib_stereo(const std::string& pathL, const std::string& pathR, const cv::Size& size, float square_size): board_size(size), length(square_size) {
		cv::glob(pathL, imgL_str);
		cv::glob(pathR, imgR_str);

		for (int i{}; i < board_size.height; ++i)
			for (int j{}; j < board_size.width; ++j)
				objp.push_back(cv::Point3f(j * length, i * length, 0));
	}

	void calibrate(cv::InputOutputArray R, cv::InputOutputArray t, cv::OutputArray E, cv::OutputArray F) {
		std::vector<std::vector<cv::Point3f>> objpoints;
		std::vector<std::vector<cv::Point2f>> imgL_points, imgR_points;
		std::vector<cv::Point2f> cornerL_pts, cornerR_pts;

		cv::Mat frameL, frameR, grayL, grayR, buffR;

		for (int i{}; i < imgL_str.size(); ++i) {
			frameL = cv::imread(imgL_str.at(i));
			frameR = cv::imread(imgR_str.at(i));

			if (frameL.size() != frameR.size())
				cv::resize(frameR, frameR, frameL.size());

			cv::cvtColor(frameL, grayL, cv::COLOR_BGR2GRAY);
			cv::cvtColor(frameR, grayR, cv::COLOR_BGR2GRAY);

			bool successL = cv::findChessboardCorners(grayL, board_size, cornerL_pts);
			bool successR = cv::findChessboardCorners(grayR, board_size, cornerR_pts);

			if (successL && successR) {
				cv::TermCriteria criteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 100, 1e-6);
				cv::Size winSize = cv::Size(5, 5);
				cv::Size zeroZone = cv::Size(-1, -1);

				cv::cornerSubPix(grayL, cornerL_pts, winSize, zeroZone, criteria);
				cv::cornerSubPix(grayR, cornerR_pts, winSize, zeroZone, criteria);

				cv::drawChessboardCorners(frameL, board_size, cornerL_pts, successL);
				cv::drawChessboardCorners(frameR, board_size, cornerR_pts, successR);

				objpoints.push_back(objp);
				imgL_points.push_back(cornerL_pts);
				imgR_points.push_back(cornerR_pts);
			}

			cv::Mat combine;
			cv::hconcat(frameL, frameR, combine);

			cv::namedWindow("Image",0);
			cv::imshow("Image", combine);
			cv::waitKey(1);
		}

		cv::destroyAllWindows();
		
		cv::Mat rotL, rotR, translL, translR;
		cv::calibrateCamera(objpoints, imgL_points, frameL.size(), camL_mat, distL_mat, rotL, translL);

		cv::calibrateCamera(objpoints, imgR_points, frameR.size(), camR_mat, distR_mat, rotR, translR);

		int flag{};
		cv::TermCriteria criteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 100, 1e-6);
		flag |= cv::CALIB_FIX_INTRINSIC;

		cv::FileStorage fsL("Artem.yaml", cv::FileStorage::READ);
		cv::FileStorage fsR("Jora.yaml", cv::FileStorage::READ);

		cv::stereoCalibrate(objpoints, imgL_points, imgR_points, camL_mat, distL_mat, camR_mat, distR_mat, frameR.size(), R, t, E, F, flag, criteria);

		Rotation = R.getMat();
		translation = t.getMat();
		Essential = E.getMat();
		Fundamental = F.getMat();
	}

	void stereo_rectification(const std::vector<std::string>& pths) {
		cv::Mat rect_l, rect_r, proj_mat_l, proj_mat_r, Q;

		cv::Mat frameL = cv::imread(pths.at(0));
		cv::Mat frameR = cv::imread(pths.at(1));

		if (frameL.size() != frameR.size())
			cv::resize(frameR, frameR, frameL.size());

		cv::namedWindow("Before", 0);

		cv::Mat combineBefore;
		cv::hconcat(frameL, frameR, combineBefore);

		cv::imshow("Before", combineBefore);
		cv::waitKey(10000);

		cv::stereoRectify(camL_mat, distL_mat, camR_mat, distR_mat, frameL.size(), Rotation, translation, rect_l, rect_r, proj_mat_l, proj_mat_r, Q, 1);
		
		cv::Mat LeftStereo_Mapx, LeftStereo_Mapy;
		cv::Mat RightStereo_Mapx, RightStereo_Mapy;

		cv::initUndistortRectifyMap(camL_mat, distL_mat, rect_l, proj_mat_l, frameL.size(), CV_16SC2, LeftStereo_Mapx, LeftStereo_Mapy);
		cv::initUndistortRectifyMap(camR_mat, distR_mat, rect_r, proj_mat_r, frameR.size(), CV_16SC2, RightStereo_Mapx, RightStereo_Mapy);

		cv::Mat Left_nice, Right_nice;
		cv::remap(frameL, Left_nice, LeftStereo_Mapx, LeftStereo_Mapy, cv::INTER_LANCZOS4, cv::BORDER_CONSTANT, 0);
		cv::remap(frameR, Right_nice, RightStereo_Mapx, RightStereo_Mapy, cv::INTER_LANCZOS4, cv::BORDER_CONSTANT, 0);

		cv::namedWindow("After", 0);

		cv::Mat combineAfter;
		cv::hconcat(Left_nice, Right_nice, combineAfter);

		cv::imshow("After", combineAfter);
		cv::waitKey(10000);

		cv::Mat Left_nice_split[3], Right_nice_split[3];

		std::vector<cv::Mat> Anaglyph_channels;

		cv::split(Left_nice, Left_nice_split);
		cv::split(Right_nice, Right_nice_split);

		Anaglyph_channels.push_back(Right_nice_split[0]);
		Anaglyph_channels.push_back(Right_nice_split[1]);
		Anaglyph_channels.push_back(Left_nice_split[2]);

		cv::Mat Anaglyph_img;
		cv::merge(Anaglyph_channels, Anaglyph_img);

		cv::imwrite("artem.jpg", Anaglyph_img);

		cv::namedWindow("My Photo 3D", 0);
		cv::imshow("My Photo 3D", Anaglyph_img);
		cv::waitKey(10000);
	}

	void save_calib(const std::string& filename) {
		cv::FileStorage fs(filename, cv::FileStorage::WRITE);

		if (!fs.isOpened()) {
			std::cerr << "Can't open the File" << std::endl;
			return; 
		}
		
		fs << "Size board" << board_size;
		fs << "Square size" << length;

		fs << "Camera Matrix Left" << camL_mat;
		fs << "Distorsion koefficient Left" << distL_mat;
		fs << "Camera Matrix Right" << camR_mat;
		fs << "Distorsion koefficient Right" << distR_mat;

		fs << "Rotation" << Rotation;
		fs << "Translation" << translation;
		fs << "Essential" << Essential;
		fs << "Fundamental" << Fundamental;

		fs.release();
	}
};

inline void calibrate_stereo() {
	calib_stereo stereo_pair("../pictures/calib/left/auditory", "../pictures/calib/right/auditory", cv::Size(9, 6), 0.024);

	cv::Mat R, t, E, F;
	stereo_pair.calibrate(R, t, E, F);

	stereo_pair.save_calib("../config/stereo_pair.yaml");
}