#pragma once

#include <opencv2/highgui.hpp>
#include <sstream>
#include <vector>
#include "detector.hpp"

class index_t {
private:
	const size_t _index;
	inline static size_t _increment_index = 0;
public:
	index_t(): _index(_increment_index) {
		++_increment_index;
	}

	size_t get_index() const {
		return _index;
	}

	std::string get_str() const {
		std::string str;
		std::stringstream stream;
		stream << _index;
		stream >> str;

		return str;
	}
};

class camera_t {
private:
	cv::Mat cam_mtx, dist_coeff;
	cv::Mat R, t;

	index_t cid;

	friend class multiply_view;
public:
	camera_t(const std::string& config): cid() {
		cv::FileStorage fs(config, cv::FileStorage::READ);

		fs["camera_matrix"] >> cam_mtx;
		fs["distortion_coefficients"] >> dist_coeff;

		fs.release();
	};

	std::vector<cv::Vec3f> getCoordSphere() const {
		return detector().red_ellipse(getImageSphere());
	}

	std::vector<cv::RotatedRect> get_param_ellipse() const{
		std::vector<std::vector<cv::Point>> ellipse;
		detector().find_ellipse(getImageSphere(), ellipse);

		std::vector<cv::RotatedRect> output;
		for(const auto& contour : ellipse)
			output.push_back(cv::fitEllipse(contour));

		return output;
	}

	cv::Mat getImageSphere() const {
		cv::Mat src = cv::imread(make_path("../../pictures/from_ue5/"));

		return !src.empty() ? src : cv::Mat();
	};

	cv::Mat getImageTarget() const {
		cv::Mat src = cv::imread(make_path("../../pictures/test_images/left/"));

		return !src.empty() ? src : cv::Mat();
	};

	cv::Mat getR() const{
		return R;
	}

	cv::Mat getT() const{
		return t;
	}

	size_t get_index() const{
		return cid.get_index();
	}

	cv::Mat get_mtx() const{
		return cam_mtx;
	}
private:
	std::string make_path(const std::string& path) const {
		return (path + cid.get_str() + "_test.png");
	}
};