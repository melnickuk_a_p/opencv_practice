#pragma once
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#define __USE_MATH_DEFINES
#include <unordered_map>
#include <cmath>
#include <vector>

class detector {
public:
	using vvec_t = std::vector<std::vector<cv::Point>>;
	using vec_t = std::vector<cv::Point>;
private:
	static const size_t min_corner_ellipse = 5;
	static const int ksize = 7;
	static const int eps = 5;

	const std::unordered_map<std::string, cv::Scalar> hsv{
		{"min_red_down", cv::Scalar(0, 10, 200)},
		{"min_red_up", cv::Scalar(10, 255, 255)},
		{"max_red_down", cv::Scalar(170, 10, 0)},
		{"max_red_up", cv::Scalar(180, 255, 255)},
	};

public:

	detector() = default;

	std::vector<cv::Point2f> black_rectangle(const cv::Mat& src) {
		// Convert to grayscale
		cv::Mat src_HSV, out;

		cv::cvtColor(src, src_HSV, cv::COLOR_BGR2HSV);

		cv::medianBlur(src_HSV, src_HSV, 5);

		int low_H = 0, low_S = 0, low_V = 0;
		int high_H = 180, high_S = 255, high_V = 90;

		cv::inRange(src_HSV, cv::Scalar(low_H, low_S, low_V), cv::Scalar(high_H, high_S, high_V), out);

		// Find contours
		std::vector<std::vector<cv::Point> > contours;
		cv::findContours(out.clone(), contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

		std::vector<cv::Point> approx;
		//cv::Mat dst = src.clone();

		std::vector<std::vector<cv::Point>> find_rectange;

		for (int i = 0; i < contours.size(); i++)
		{
			// Approximate contour with accuracy proportional
			// to the contour perimeter
			cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true) * 0.02, true);

			// Skip small or non-convex objects 
			if (std::fabs(cv::contourArea(contours[i])) > 1e8 || !cv::isContourConvex(approx) || std::fabs(cv::contourArea(contours[i])) < 1e4)
				continue;

			if (approx.size() >= 4)
				find_rectange.push_back(approx);
		}

		std::vector<cv::Rect> rects;
		for (const auto contour : find_rectange)
			rects.push_back(cv::boundingRect(contour));

		int index = 0;
		int max_index = 0;
		float max = 0.0f;

		for (const auto rect : rects) {
			//running in rect

			int count = 0;

			for (int i = rect.x; i < rect.x + rect.width; ++i) {
				for (int j = rect.y; j < rect.y + rect.height; ++j) {

					if ((int)src.at<cv::Vec3b>(cv::Point(i, j))[2] < 50)
						++count;
				}
			}

			float frac = (float)count / (rect.width * rect.height);

			if (frac > max) {
				max_index = index;
				max = frac;
			}

			++index;
		}

		int center_x = float(find_rectange[max_index][0].x + find_rectange[max_index][1].x + find_rectange[max_index][2].x + find_rectange[max_index][3].x) / 4.0;
		int center_y = float(find_rectange[max_index][0].y + find_rectange[max_index][1].y + find_rectange[max_index][2].y + find_rectange[max_index][3].y) / 4.0;

		std::vector<cv::Point> points;
		for (auto& corner : find_rectange[max_index])
			points.push_back(corner);

		points.push_back(((points.at(0) + points.at(1)) / 2));
		points.push_back(((points.at(1) + points.at(2)) / 2));
		points.push_back(((points.at(2) + points.at(3)) / 2));
		points.push_back(((points.at(0) + points.at(3)) / 2));
		points.push_back(cv::Point(center_x, center_y));

		std::vector<cv::Point2f> result(points.begin(), points.end());

		return result;
	}

	std::vector<cv::Vec3f> red_ellipse(cv::InputArray src) const {
		cv::Mat gray;
		cv::cvtColor(src, gray, cv::COLOR_BGR2GRAY);
		cv::medianBlur(gray, gray, ksize);
		std::vector<cv::Vec3f> circles;
		cv::HoughCircles(gray, circles, cv::HOUGH_GRADIENT, 1,
			gray.rows / 8,
			100, 30,
			1, 10000
		);

		return circles;
	}

	void find_ellipse(cv::InputOutputArray src, vvec_t& detecting_ellipse) {
		if (src.empty())
			return;

		cv::Mat dst;
		prepare_image(src, dst);

		vvec_t contours;
		cv::findContours(dst, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

		cv::Mat cont = src.getMat().clone();
		cv::drawContours(cont, contours, -1, cv::Scalar(0,255,0), 3);

		match_target_ellipse(src, contours, detecting_ellipse);
	}

private:

	void prepare_image(cv::InputArray src, cv::OutputArray dst) {
		cv::Mat src_hsv;
		cv::cvtColor(src, src_hsv, cv::COLOR_BGR2HSV);

		cv::Mat blur;
		cv::medianBlur(src_hsv, blur, ksize);

		cv::Mat mask_lower, mask_upper;
		cv::inRange(blur, hsv.at("min_red_down"), hsv.at("min_red_up"), mask_lower);
		cv::inRange(blur, hsv.at("max_red_down"), hsv.at("max_red_up"), mask_upper);

		cv::bitwise_or(mask_lower, mask_upper, dst);
	}

	void match_target_ellipse(cv::InputArray src, const vvec_t& contours, vvec_t& detected) const {
		for (const auto& contour : contours)
			if (condition_for_ellipse(src, contour))
				detected.push_back(contour);
	}

	bool condition_for_ellipse(cv::InputArray src, const vec_t& contour) const {
		// return (contour.size() >= min_corner_ellipse && get_square_ellipse(contour) > 500)
		// 	&& (mean_color(src, contour)[0] < hsv.at("min_red_up")[0]-eps || mean_color(src, contour)[0]>hsv.at("max_red_down")[0]+eps);
		return (contour.size() >= min_corner_ellipse) && get_square_ellipse(contour)>600 && (mean_color(src, contour)[0] > 0.90) && get_square_ellipse(contour)<100000;
	}

	float get_square_ellipse(const vec_t& contour) const {
		auto box = cv::minAreaRect(contour);

		return M_PI * box.size.width/2.0 * box.size.height/2.0;
	}

	cv::Scalar mean_color(cv::InputArray src, const vec_t& contour) const {
		cv::Mat mask = cv::Mat::zeros(src.size(), CV_8UC1);
		
		vvec_t contours{contour};
		cv::drawContours(mask, contours, -1, 255, -1);

		cv::Mat src_hsv;
		cv::cvtColor(src, src_hsv, cv::COLOR_BGR2HSV);

		cv::Mat blur;
		cv::medianBlur(src_hsv, blur, ksize);

		cv::Mat mask_lower, mask_upper;
		cv::inRange(blur, hsv.at("min_red_down"), hsv.at("min_red_up"), mask_lower);
		cv::inRange(blur, hsv.at("max_red_down"), hsv.at("max_red_up"), mask_upper);

		cv::Mat mmask_lower;
		cv::bitwise_and(mask_lower, mask, mmask_lower);
		
		cv::Mat mmask_up;
		cv::bitwise_and(mask_upper, mask, mmask_up);

		cv::Mat mmask;
		cv::bitwise_or(mmask_lower, mmask_up , mmask);

		cv::Scalar mean_lower = cv::mean(src_hsv, mmask_lower);
		cv::Scalar mean_up = cv::mean(src_hsv, mmask_up);

		return (mean_lower+mean_up)/180.0;
	}
};