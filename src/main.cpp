#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include <opencv2\opencv.hpp>
#include <opencv2\features2d.hpp>

#include "src\camera.hpp"
#include "src\utils.hpp"
#include "src\detector.hpp"
#include "src\calib_stereo.hpp"
#include "src\calib_camera.hpp"
#include "src\multiply_view.hpp"




void pixel_1to2(const cv::Mat& Kb, const cv::Point2f& pb, const cv::Mat& Ka, const cv::Mat& R, const cv::Mat& t, cv::Point2f& pa, bool orientation = false) {

	cv::Vec<double, 3> PB{ double(pb.x), double(pb.y), 1.0 };
	cv::Mat PA;

	cv::Mat one{ cv::Mat::eye(3,3,CV_64F) };
	cv::hconcat(one, cv::Vec<double, 3> ( 0.0, 0.0, 0.0 ), one);

	cv::Mat invOne = one.t();
	

	cv::Mat invKb;
	cv::invert(Kb, invKb);

	cv::Mat rowZero = (cv::Mat_<double>(1, 4) << 0.0, 0.0, 0.0, 1.0);
	
	if (orientation) {
		cv::Mat M_1to2(R);
		cv::hconcat(M_1to2, t, M_1to2);
		cv::vconcat(M_1to2, rowZero, M_1to2);

		PA = Ka * one * M_1to2 * invOne * invKb * PB;
	}
	else {
		cv::Mat M_2to1(R.t());
		cv::hconcat(M_2to1, -R.t()*t + t, M_2to1);
		cv::vconcat(M_2to1, rowZero, M_2to1);

		PA = Ka * one * M_2to1 * invOne * invKb * PB;
	}
	
	pa.x = static_cast<float>(-PA.at<double>(0, 0) / PA.at<double>(2, 0));
	pa.y = static_cast<float>(PA.at<double>(1, 0) / PA.at<double>(2, 0));
}

void Homo(const cv::Mat& Kb, const cv::Point2f& pb, const cv::Mat& Ka, const cv::Mat& R, const cv::Mat& t, 
	const cv::Mat& Ra, const cv::Mat& ta, cv::Point2f& pa) {
	
	cv::Vec<double, 3> Pb{ double(pb.x), double(pb.y), 1.0 };
	
	cv::Mat M_1to2;
	cv::hconcat(R, t, M_1to2);

	cv::Mat rowZero = (cv::Mat_<double>(1, 1) << 0.0);

	cv::Mat normal = (cv::Mat_<double>(3, 1) << 0, 0, 1);
	cv::Mat n = Ra * normal;
	cv::vconcat(n, cv::Vec<double, 1>(0.0), n);

	cv::Mat origin(3, 1, CV_64F, cv::Scalar(0));
	origin = Ra * origin + ta;
	cv::vconcat(origin, cv::Vec<double, 1>(0.0), origin);


	cv::Mat normalB = M_1to2 * n;
	cv::Mat originB = M_1to2 * origin;

	cv::Mat_<double> nb(3, 1);
	cv::Mat_<double> origin_B(3, 1);

	nb.at<double>(0, 0) = normalB.at<double>(0, 0);
	nb.at<double>(0, 1) = normalB.at<double>(0, 1);
	nb.at<double>(0, 2) = normalB.at<double>(0, 2);

	origin_B.at<double>(0, 0) = originB.at<double>(0, 0);
	origin_B.at<double>(0, 1) = originB.at<double>(0, 1);
	origin_B.at<double>(0, 2) = originB.at<double>(0, 2);

	double d_invb = 1/nb.dot(origin_B);

	std::cout << "origin_B: " << originB.at<double>(0, 3) << std::endl;

	cv::Mat homography = R + d_invb * (t) * nb.t();
	cv::Mat P1;

	P1 = Ka * homography * Kb.inv() * Pb;

	pa.x = static_cast<float>(P1.at<double>(0, 0) / P1.at<double>(2, 0));
	pa.y = static_cast<float>(P1.at<double>(1, 0) / P1.at<double>(2, 0));

}

void pixel_1to2Homo(const cv::Mat& Kb, const cv::Mat& Rb, const cv::Mat& tb, const cv::Point2f& pb,
	const cv::Mat& Ka, const cv::Mat& Ra, const cv::Mat& ta, cv::Point2f& pa, const cv::Mat_<double> _3dcoord) {
	
	cv::Vec<double, 3> Pb{ double(pb.x), double(pb.y), 1.0 };

	cv::Mat rowZero = (cv::Mat_<double>(1, 4) << 0.0, 0.0, 0.0, 1);

	cv::Mat_<double> M_0toa(Ra);
	cv::hconcat(M_0toa, ta, M_0toa);
	cv::vconcat(M_0toa, rowZero, M_0toa);

	cv::Mat_<double> M_0tob(Rb);
	cv::hconcat(M_0tob, tb, M_0tob);
	cv::vconcat(M_0tob, rowZero, M_0tob);

	cv::Mat PA, PB;
	PA = M_0toa * _3dcoord;
	PB = M_0tob * _3dcoord;

	double za = PA.at<double>(2, 0) / PA.at<double>(3, 0);
	double zb = PB.at<double>(2, 0) / PB.at<double>(3, 0);


	cv::Mat R_btoa = Ra * Rb.t();
	cv::Mat t_1to2 = Ra * (-Rb.t() * tb) + ta;

	cv::Mat normal = (cv::Mat_<double>(3, 1) << 0, 0, 1);
	cv::Mat n1 = Rb * normal;
	
	cv::Mat origin(3, 1, CV_64F, cv::Scalar(0));
	cv::Mat originb = Rb * origin + tb;
	double d_invb = 1.0 / n1.dot(originb);

	cv::Mat homography = R_btoa + d_invb * t_1to2 * normal.t();
	cv::Mat P1;

	P1 = zb/za * Ka * homography * Kb.inv() * Pb;

	pa.x = static_cast<float>(P1.at<double>(0, 0) / P1.at<double>(2, 0));
	pa.y = static_cast<float>(P1.at<double>(1, 0) / P1.at<double>(2, 0));
}


void my_test() {
	size_t numb_cams = 3;
	std::vector<std::shared_ptr<camera_t>> cam_pts;
	for (size_t i{}; i < numb_cams; ++i)
		cam_pts.push_back(std::make_shared<camera_t>("./config/cam_ue5.yaml"));

	multiply_view local_view(cam_pts);

	local_view.set_relative_transform();

	cv::Mat image0 = cam_pts.at(0)->getImageSphere();
	std::vector<cv::Vec3f> sphere0 = cam_pts.at(0)->getCoordSphere();

	cv::Mat image1 = cam_pts.at(1)->getImageSphere();
	std::vector<cv::Vec3f> sphere1 = cam_pts.at(1)->getCoordSphere();

	cv::Mat image2 = cam_pts.at(2)->getImageSphere();
	std::vector<cv::Vec3f> sphere2 = cam_pts.at(2)->getCoordSphere();

	cv::Mat image3 = cam_pts.at(2)->getImageSphere();

	unsigned short n1{ 1 }, n2{ 2 };
	cv::Vec2f center0 = cv::Vec2f(sphere0.at(n1)[0], sphere0.at(n1)[1]);
	cv::Vec2f center1 = cv::Vec2f(sphere1.at(n2)[0], sphere0.at(n2)[1]);

	cv::Mat _3dCoordCenterStr = method_triangulate(center0, center1, cam_pts.at(1)->getR(), cam_pts.at(1)->getT(),
		cam_pts.at(1)->getIternalMatx(), cam_pts.at(0)->getIternalMatx());
	cv::Mat_<double> _3dCoordCenterCol = _3dCoordCenterStr.t();
	
	std::vector<cv::Vec2f> pointsL;
	pointsL.push_back(cv::Vec2f(-sphere0.at(n1)[2] + sphere0.at(n1)[0], sphere0.at(n1)[2] + sphere0.at(n1)[1]));
	pointsL.push_back(cv::Vec2f(sphere0.at(n1)[2] + sphere0.at(n1)[0], sphere0.at(n1)[2] + sphere0.at(n1)[1]));
	pointsL.push_back(cv::Vec2f(sphere0.at(n1)[2] + sphere0.at(n1)[0], -sphere0.at(n1)[2] + sphere0.at(n1)[1]));
	pointsL.push_back(cv::Vec2f(-sphere0.at(n1)[2] + sphere0.at(n1)[0], -sphere0.at(n1)[2] + sphere0.at(n1)[1]));

	std::vector<cv::Vec2f> pointsR;
	pointsR.push_back(cv::Vec2f(-sphere1.at(n2)[2] + sphere1.at(n2)[0], sphere1.at(n2)[2] + sphere1.at(n2)[1]));
	pointsR.push_back(cv::Vec2f(sphere1.at(n2)[2] + sphere1.at(n2)[0], sphere1.at(n2)[2] + sphere1.at(n2)[1]));
	pointsR.push_back(cv::Vec2f(sphere1.at(n2)[2] + sphere1.at(n2)[0], -sphere1.at(n2)[2] + sphere1.at(n2)[1]));
	pointsR.push_back(cv::Vec2f(-sphere1.at(n2)[2] + sphere1.at(n2)[0], -sphere1.at(n2)[2] + sphere1.at(n2)[1]));

	cv::Mat _3dCoordRects = method_triangulate(pointsL, pointsR, cam_pts.at(1)->getR(), cam_pts.at(1)->getT(),
		cam_pts.at(1)->getIternalMatx(), cam_pts.at(0)->getIternalMatx());

	cv::Mat_<double> _3dCoordPoint0(4, 1), _3dCoordPoint1(4, 1), _3dCoordPoint2(4, 1), _3dCoordPoint3(4, 1);
	_3dCoordPoint0 = _3dCoordRects.row(0).t();
	_3dCoordPoint1 = _3dCoordRects.row(1).t();
	_3dCoordPoint2 = _3dCoordRects.row(2).t();
	_3dCoordPoint3 = _3dCoordRects.row(3).t();

	std::vector<cv::Point3d> obj_points;
	obj_points.push_back(cv::Point3d(_3dCoordPoint0.at<double>(0, 0), _3dCoordPoint0.at<double>(0, 1), _3dCoordPoint0.at<double>(0, 2)));
	obj_points.push_back(cv::Point3d(_3dCoordPoint1.at<double>(0, 0), _3dCoordPoint1.at<double>(0, 1), _3dCoordPoint1.at<double>(0, 2)));
	obj_points.push_back(cv::Point3d(_3dCoordPoint2.at<double>(0, 0), _3dCoordPoint2.at<double>(0, 1), _3dCoordPoint2.at<double>(0, 2)));
	obj_points.push_back(cv::Point3d(_3dCoordPoint3.at<double>(0, 0), _3dCoordPoint3.at<double>(0, 1), _3dCoordPoint3.at<double>(0, 2)));

	cv::Mat R_vect, t, R;
	cv::solvePnP(obj_points, pointsR, cam_pts.at(1)->getIternalMatx(), cam_pts.at(1)->getDistCoeff(), R_vect, t, false, cv::SOLVEPNP_AP3P);
	cv::Rodrigues(R_vect, R);

	cv::Mat rowZero = (cv::Mat_<double>(1, 4) << 0.0, 0.0, 0.0, 1.0);
	cv::Mat one{ cv::Mat::eye(3,3,CV_64F) };
	cv::hconcat(one, cv::Vec<double, 3>(0.0, 0.0, 0.0), one);

	cv::Mat P1;
	cv::hconcat(R, t, P1);
	cv::vconcat(P1, rowZero, P1);	

	cv::Mat P, P_0to2, P_0to1;
	cv::hconcat(cam_pts.at(2)->getR(), cam_pts.at(2)->getT(), P_0to2);
	cv::vconcat(P_0to2, rowZero, P_0to2);

	cv::hconcat(cam_pts.at(1)->getR(), cam_pts.at(1)->getT(), P_0to1);
	cv::vconcat(P_0to1, rowZero, P_0to1);

	P = cam_pts.at(2)->getIternalMatx() * one * P_0to2;

	cv::Mat P2 = P_0to1.inv() * P1;
	
	cv::Mat center10, out10, out11, out12, out13;
	center10 = P * P2.inv() * _3dCoordCenterCol;
	out10 = P * _3dCoordPoint0;
	out11 = P * _3dCoordPoint1;
	out12 = P * _3dCoordPoint2;
	out13 = P * _3dCoordPoint3;

	cv::Point2d resCenter1, res10, res11, res12, res13;
	resCenter1 = cv::Point2d(center10.at<double>(0, 0) / center10.at<double>(0, 2), center10.at<double>(0, 1) / center10.at<double>(0, 2));
	res10 = cv::Point2d(out10.at<double>(0, 0) / out10.at<double>(0, 2), out10.at<double>(0, 1) / out10.at<double>(0, 2));
	res11 = cv::Point2d(out11.at<double>(0, 0) / out11.at<double>(0, 2), out11.at<double>(0, 1) / out11.at<double>(0, 2));
	res12 = cv::Point2d(out12.at<double>(0, 0) / out12.at<double>(0, 2), out12.at<double>(0, 1) / out12.at<double>(0, 2));
	res13 = cv::Point2d(out13.at<double>(0, 0) / out13.at<double>(0, 2), out13.at<double>(0, 1) / out13.at<double>(0, 2));

	cv::circle(image3, resCenter1, 10, cv::Scalar(0, 125, 125), 2);
	cv::line(image3, res10, res11, cv::Scalar(0, 125, 125), 2);
	cv::line(image3, res11, res12, cv::Scalar(0, 125, 125), 2);
	cv::line(image3, res12, res13, cv::Scalar(0, 125, 125), 2);
	cv::line(image3, res13, res10, cv::Scalar(0, 125, 125), 2);

	double d1 = sqrt((res10.x - res12.x) * (res10.x - res12.x) + (res10.y - res12.y) * (res10.y - res12.y));
	double d2 = sqrt((res11.x - res13.x) * (res11.x - res13.x) + (res11.y - res13.y) * (res11.y - res13.y));

	double side = (d1 < d2) ? (d2 / 2) : (d1 / 2);

	res10 = cv::Point2d(resCenter1.x - side, resCenter1.y + side);
	res11 = cv::Point2d(resCenter1.x + side, resCenter1.y + side);
	res12 = cv::Point2d(resCenter1.x + side, resCenter1.y - side);
	res13 = cv::Point2d(resCenter1.x - side, resCenter1.y - side);

	cv::circle(image3, resCenter1, side*1, cv::Scalar(255, 255, 0), 2);
	
	cv::Mat mask(image2.size(), CV_8UC1, cv::Scalar(0, 0, 0));
	cv::Mat searchAreaImage;
	std::vector<cv::Vec3f> sphereFinded;
	std::vector<std::vector<cv::Point>> sF;
	cv::RotatedRect rotRect;
	float k{ 1 };
	while (true) {
		cv::circle(mask, resCenter1, side * k, cv::Scalar(255, 255, 255), -1);
		cv::copyTo(image2, searchAreaImage, mask);
		sphereFinded = detector().red_ellipse(searchAreaImage);
		detector().find_ellipse(searchAreaImage, sF);
		rotRect = cv::fitEllipse(sF.at(0));
		std::cout << "k = " << k << std::endl;
		if (!sphereFinded.empty()) {
			cv::circle(image3, cv::Point(sphereFinded.at(0)[0], sphereFinded.at(0)[1]), sphereFinded.at(0)[2], cv::Scalar(0, 0, 255), 2);
			cv::drawContours(image3, sF, -1, cv::Scalar(125, 0, 255), -1);
			break;
		}
		k += 0.25;
		
	}
	std::cout << "res0" << res10 << std::endl;
	std::cout << "res1" << res11 << std::endl;
	std::cout << "res2" << res12 << std::endl;
	std::cout << "res3" << res13 << std::endl;

	cv::namedWindow("Res", 0);
	cv::imshow("Res", searchAreaImage);
	
	cv::namedWindow("Res1", 0);
	cv::imshow("Res1", image3);

	
	cv::waitKey();
}


//void my_test2() {
//	size_t numb_cams = 3;
//	std::vector<std::shared_ptr<camera_t>> cam_pts;
//	for (size_t i{}; i < numb_cams; ++i)
//		cam_pts.push_back(std::make_shared<camera_t>("./config/cam_ue5.yaml"));
//
//	multiply_view local_view(cam_pts);
//
//	local_view.set_relative_transform();
//
//	cv::Mat image1 = cam_pts.at(0)->getImageSphere();
//	std::vector<cv::Vec3f> sphere0 = cam_pts.at(0)->getCoordSphere();
//
//	for (const auto& point : sphere0)
//		cv::circle(image1, cv::Point2f(point[0], point[1]), point[2], cv::Scalar(0, 0, 255), 5);
//
//	cv::Mat image2 = cam_pts.at(1)->getImageSphere();
//	std::vector<cv::Vec3f> sphere2 = cam_pts.at(1)->getCoordSphere();
//
//	const int numberSphere = 0;
//
//	for (const auto& point : sphere2)
//		cv::circle(image2, cv::Point2f(point[0], point[1]), point[2], cv::Scalar(0, 0, 255), 5);
//
//	cv::Vec2f pointL(sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[1]);
//	cv::Vec2f pointR(sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[1]);
//
//	std::vector<cv::Vec2f> pointForSloveL;
//	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[1]));
//	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0] + sphere0.at(numberSphere)[2], sphere0.at(numberSphere)[1]));
//	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[1] + sphere0.at(numberSphere)[2]));
//	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0] + sphere0.at(numberSphere)[2], sphere0.at(numberSphere)[1] + sphere0.at(numberSphere)[2]));
//
//	std::vector<cv::Vec2f> pointForSloveR;
//	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[1]));
//	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0] + sphere2.at(numberSphere)[2], sphere2.at(numberSphere)[1]));
//	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[1] + sphere2.at(numberSphere)[2]));
//	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0] + sphere2.at(numberSphere)[2], sphere2.at(numberSphere)[1] + sphere2.at(numberSphere)[2]));
//
//	float r = 1.0f;
//
//	std::vector<cv::Point3f> obj_points;
//	obj_points.push_back(cv::Point3f(0.0, 0.0, 0.0));
//	obj_points.push_back(cv::Point3f(r, 0.0, 0.0));
//	obj_points.push_back(cv::Point3f(0.0, r, 0.0));
//	obj_points.push_back(cv::Point3f(r, r, 0.0));
//
//	cv::Mat R_left_vec, R_left, t_left;
//	cv::solvePnP(obj_points, pointForSloveL, cam_pts.at(0)->getIternalMatx(), cam_pts.at(0)->getDistCoeff(), R_left_vec, t_left, false, cv::SOLVEPNP_AP3P);
//	cv::Rodrigues(R_left_vec, R_left);
//
//	cv::Mat R_right_vec, R_right, t_right;
//	cv::solvePnP(obj_points, pointForSloveR, cam_pts.at(1)->getIternalMatx(), cam_pts.at(1)->getDistCoeff(), R_right_vec, t_right, false, cv::SOLVEPNP_AP3P);
//	cv::Rodrigues(R_right_vec, R_right);
//
//	cv::Mat res = method_triangulate(pointL, pointR, cam_pts.at(1)->getR(), cam_pts.at(1)->getT(), cam_pts.at(1)->getIternalMatx(), cam_pts.at(0)->getIternalMatx());
//	cv::Mat_<double> out(4, 1);
//	out.at<double>(cv::Point2d(0, 0)) = static_cast<double>(res.at<float>(cv::Point2f(0, 0)));
//	out.at<double>(cv::Point2d(0, 1)) = static_cast<double>(res.at<float>(cv::Point2f(1, 0)));
//	out.at<double>(cv::Point2d(0, 2)) = static_cast<double>(res.at<float>(cv::Point2f(2, 0)));
//	out.at<double>(cv::Point2d(0, 3)) = static_cast<double>(res.at<float>(cv::Point2f(3, 0)));
//
//	std::cout << res << std::endl;
//
//	std::vector<cv::Point2f> rec;
//	rec.push_back(cv::Point2f(-sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
//	rec.push_back(cv::Point2f(sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
//	rec.push_back(cv::Point2f(sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], -sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
//	rec.push_back(cv::Point2f(-sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], -sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
//
//	std::vector<cv::Point2f> rec2;
//	rec2.push_back(cv::Point2f(-sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
//	rec2.push_back(cv::Point2f(sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
//	rec2.push_back(cv::Point2f(sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], -sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
//	rec2.push_back(cv::Point2f(-sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], -sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
//
//	std::vector<cv::Point2f> output;
//	cv::Point2f buff;
//	for (const auto point : rec) {
//		pixel_1to2Homo(cam_pts.at(0)->getIternalMatx(), R_left, t_left, point,
//			cam_pts.at(1)->getIternalMatx(), R_right, t_right, buff, out);
//		output.push_back(buff);
//	}
//
//	std::vector<cv::Point2f> output1;
//	cv::Point2f buff1;
//	for (const auto point : rec) {
//
//		Homo(cam_pts.at(0)->getIternalMatx(), point, cam_pts.at(1)->getIternalMatx(), cam_pts.at(1)->getR(), cam_pts.at(1)->getT(),
//			R_left, t_left, buff1);
//
//		output1.push_back(buff1);
//		std::cout << buff1 << std::endl;
//	}
//
//	cv::rectangle(image1, rec.at(0), rec.at(2), cv::Scalar(0, 255, 0), 5);
//	cv::rectangle(image2, rec2.at(0), rec2.at(2), cv::Scalar(0, 255, 0), 5);
//	//cv::rectangle(image2, output.at(0), output.at(2), cv::Scalar(255, 0, 255), 2);
//	cv::line(image2, output.at(0), output.at(1), cv::Scalar(255, 0, 255), 2);
//	cv::line(image2, output.at(1), output.at(2), cv::Scalar(255, 0, 255), 2);
//	cv::line(image2, output.at(2), output.at(3), cv::Scalar(255, 0, 255), 2);
//	cv::line(image2, output.at(3), output.at(0), cv::Scalar(255, 0, 255), 2);
//
//	cv::line(image2, output1.at(0), output1.at(1), cv::Scalar(125, 0, 125), 2);
//	cv::line(image2, output1.at(1), output1.at(2), cv::Scalar(125, 0, 125), 2);
//	cv::line(image2, output1.at(2), output1.at(3), cv::Scalar(125, 0, 125), 2);
//	cv::line(image2, output1.at(3), output1.at(0), cv::Scalar(125, 0, 125), 2);
//
//	cv::Mat image;
//	cv::hconcat(image1, image2, image);
//
//	cv::namedWindow("cam", 0);
//	cv::imshow("cam", image);
//	cv::waitKey();
//}


int main() {
	my_test();
	return 0;
}
/*
int main(int argn, char* argc[]) {
	size_t numb_cams = 3;
	std::vector<std::shared_ptr<camera_t>> cam_pts;
	for (size_t i{}; i < numb_cams; ++i)
		cam_pts.push_back(std::make_shared<camera_t>("./config/cam_ue5.yaml"));

	multiply_view local_view(cam_pts);

	local_view.set_relative_transform();

	cv::Mat image1 = cam_pts.at(0)->getImageSphere();
	std::vector<cv::Vec3f> sphere0 = cam_pts.at(0)->getCoordSphere();

	for (const auto& point : sphere0)
		cv::circle(image1, cv::Point2f(point[0], point[1]), point[2], cv::Scalar(0, 0, 255), 5);

	cv::Mat image2 = cam_pts.at(1)->getImageSphere();
	std::vector<cv::Vec3f> sphere2 = cam_pts.at(1)->getCoordSphere();

	const int numberSphere = 0;

	for (const auto& point : sphere2)
		cv::circle(image2, cv::Point2f(point[0], point[1]), point[2], cv::Scalar(0, 0, 255), 5);

	cv::Vec2f pointL(sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[1]);
	cv::Vec2f pointR(sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[1]);

	std::vector<cv::Vec2f> pointForSloveL;
	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[1]));
	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0] + sphere0.at(numberSphere)[2], sphere0.at(numberSphere)[1]));
	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[1] + sphere0.at(numberSphere)[2]));
	pointForSloveL.push_back(cv::Point2f(sphere0.at(numberSphere)[0] + sphere0.at(numberSphere)[2], sphere0.at(numberSphere)[1] + sphere0.at(numberSphere)[2]));

	std::vector<cv::Vec2f> pointForSloveR;
	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[1]));
	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0] + sphere2.at(numberSphere)[2], sphere2.at(numberSphere)[1]));
	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[1] + sphere2.at(numberSphere)[2]));
	pointForSloveR.push_back(cv::Point2f(sphere2.at(numberSphere)[0] + sphere2.at(numberSphere)[2], sphere2.at(numberSphere)[1] + sphere2.at(numberSphere)[2]));
	
	


	float r = 1.0f;

	std::vector<cv::Point3f> obj_points;
	obj_points.push_back(cv::Point3f(0.0, 0.0, 0.0));
	obj_points.push_back(cv::Point3f(r, 0.0, 0.0));
	obj_points.push_back(cv::Point3f(0.0, r, 0.0));
	obj_points.push_back(cv::Point3f(r, r, 0.0));

	cv::Mat R_left_vec, R_left, t_left;
	cv::solvePnP(obj_points, pointForSloveL, cam_pts.at(0)->getIternalMatx(), cam_pts.at(0)->getDistCoeff(), R_left_vec, t_left, false, cv::SOLVEPNP_AP3P);
	cv::Rodrigues(R_left_vec, R_left);

	cv::Mat R_right_vec, R_right, t_right;
	cv::solvePnP(obj_points, pointForSloveR, cam_pts.at(1)->getIternalMatx(), cam_pts.at(1)->getDistCoeff(), R_right_vec, t_right, false, cv::SOLVEPNP_AP3P);
	cv::Rodrigues(R_right_vec, R_right);
	
	cv::Mat res = method_triangulate(pointL, pointR, cam_pts.at(1)->getR(), cam_pts.at(1)->getT(), cam_pts.at(1)->getIternalMatx(), cam_pts.at(0)->getIternalMatx());
	cv::Mat_<double> out(4, 1);
	out.at<double>(cv::Point2d(0, 0)) = static_cast<double>(res.at<float>(cv::Point2f(0,0)));
	out.at<double>(cv::Point2d(0, 1)) = static_cast<double>(res.at<float>(cv::Point2f(1,0)));
	out.at<double>(cv::Point2d(0, 2)) = static_cast<double>(res.at<float>(cv::Point2f(2,0)));
	out.at<double>(cv::Point2d(0, 3)) = static_cast<double>(res.at<float>(cv::Point2f(3,0)));

	std::cout << res << std::endl;

	std::vector<cv::Point2f> rec;
	rec.push_back(cv::Point2f(-sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
	rec.push_back(cv::Point2f(sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
	rec.push_back(cv::Point2f(sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], -sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));
	rec.push_back(cv::Point2f(-sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[0], -sphere0.at(numberSphere)[2] + sphere0.at(numberSphere)[1]));

	std::vector<cv::Point2f> rec2;
	rec2.push_back(cv::Point2f(-sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
	rec2.push_back(cv::Point2f(sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
	rec2.push_back(cv::Point2f(sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], -sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));
	rec2.push_back(cv::Point2f(-sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[0], -sphere2.at(numberSphere)[2] + sphere2.at(numberSphere)[1]));

	std::vector<cv::Point2f> output;
	cv::Point2f buff;
	for (const auto point : rec) {
		//pixel_1to2(cam_pts.at(1)->getIternalMatx(), point, cam_pts.at(0)->getIternalMatx(), cam_pts.at(1)->getR(), cam_pts.at(1)->getT(), buff, true);
		//Homo(cam_pts.at(0)->getIternalMatx(), point, cam_pts.at(1)->getIternalMatx(), cam_pts.at(1)->getR(), cam_pts.at(1)->getT(), out, buff);
		
		pixel_1to2Homo(cam_pts.at(0)->getIternalMatx(), R_left, t_left, point,
			cam_pts.at(1)->getIternalMatx(), R_right, t_right, buff, out);
		output.push_back(buff);
	}

	std::vector<cv::Point2f> output1;
	cv::Point2f buff1;
	for (const auto point : rec) {
		
		Homo(cam_pts.at(0)->getIternalMatx(), point, cam_pts.at(1)->getIternalMatx(), cam_pts.at(1)->getR(), cam_pts.at(1)->getT(), 
			R_left, t_left, buff1);

		output1.push_back(buff1);
		std::cout << buff1 << std::endl;
	}

	cv::rectangle(image1, rec.at(0), rec.at(2), cv::Scalar(0, 255, 0), 5);
	cv::rectangle(image2, rec2.at(0), rec2.at(2), cv::Scalar(0, 255, 0), 5);
	//cv::rectangle(image2, output.at(0), output.at(2), cv::Scalar(255, 0, 255), 2);
	cv::line(image2, output.at(0), output.at(1), cv::Scalar(255, 0, 255), 2);
	cv::line(image2, output.at(1), output.at(2), cv::Scalar(255, 0, 255), 2);
	cv::line(image2, output.at(2), output.at(3), cv::Scalar(255, 0, 255), 2);
	cv::line(image2, output.at(3), output.at(0), cv::Scalar(255, 0, 255), 2);

	cv::line(image2, output1.at(0), output1.at(1), cv::Scalar(125, 0, 125), 2);
	cv::line(image2, output1.at(1), output1.at(2), cv::Scalar(125, 0, 125), 2);
	cv::line(image2, output1.at(2), output1.at(3), cv::Scalar(125, 0, 125), 2);
	cv::line(image2, output1.at(3), output1.at(0), cv::Scalar(125, 0, 125), 2);

	



	cv::Mat image;
	cv::hconcat(image1, image2, image);

	cv::namedWindow("cam", 0);
	cv::imshow("cam", image);
	cv::waitKey();

	return 0;
}
*/