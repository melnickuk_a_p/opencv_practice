#pragma once
#include "camera.hpp"
#include "detector.hpp"
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <sstream>
#include <string>

inline Eigen::Matrix3d cv2eigen(const cv::Mat& input) {
	Eigen::Matrix3d out;
	
	for (int i{}; i < input.cols; ++i)
		for (int j{}; j < input.rows; ++j)
			out(i, j) = input.at<double>(cv::Point(j, i));

	return out;
}

inline cv::Mat eigen2cv(const Eigen::Matrix3d& input) {
	cv::Mat out(input.rows(), input.cols(), CV_64F);

	for (int i{}; i < input.cols(); ++i)
		for (int j{}; j < input.rows(); ++j)
			out.at<double>(cv::Point(j, i)) = input(i, j);
	
	return out;
}

class multiply_view {
private:
	std::vector<std::shared_ptr<camera_t>> cam_pts;

	const double r_sphere = 0.5;
public:
	multiply_view(const std::vector<std::shared_ptr<camera_t>>& _pts): cam_pts(_pts) {};

	void set_relative_transform() {
		
		bool first = true;
		
		std::vector<std::vector<cv::Vec3d>> output;
		for (const auto& cam : cam_pts){
			
			if(first){
				output.push_back(ellipse_coords(cam->get_param_ellipse(), cam->cid.get_index()));
				first = false;
			}else{
				std::vector<cv::Vec3d> buff;
				buff = ellipse_coords(cam->get_param_ellipse(), cam->cid.get_index());
				std::swap(buff.at(1), buff.at(2));
				output.push_back(buff);
				
				first = true;

			}
		}

		std::vector<cv::Mat> H;
		for (int i{}; i < output.size(); ++i) {
			auto avg = avg_r(output.at(i));
			cv::Mat buff(3, 3, CV_64F);
			for (int j{}; j < output.at(i).size(); ++j) {
				auto r_minus = output.at(i).at(j) - avg;

				buff.at<double>(cv::Point(j, 0)) = r_minus[0];
				buff.at<double>(cv::Point(j, 1)) = r_minus[1];
				buff.at<double>(cv::Point(j, 2)) = r_minus[2];
			}
			H.push_back(buff);
		}

		cam_pts.at(0)->R = cv::Mat::eye(3, 3, CV_64F);
		cam_pts.at(0)->t = cv::Mat::zeros(3, 1, CV_64F);
		
		for (int i = 1; i < H.size(); ++i) {
			cv::Mat HH = H.at(0) * H.at(i).t();

			Eigen::Matrix3d HH_eigen = cv2eigen(HH);
			Eigen::JacobiSVD<Eigen::Matrix3d> svd(HH_eigen, Eigen::ComputeFullU | Eigen::ComputeFullV);
			
			Eigen::Matrix3d V = svd.matrixV();
			Eigen::Matrix3d U = svd.matrixU();
			if (auto matrix = svd.matrixV() * svd.matrixU().transpose(); matrix.determinant() == -1.0) {
				V.coeffRef(0, 2) = -V.coeffRef(0, 2);
				V.coeffRef(1, 2) = -V.coeffRef(1, 2);
				V.coeffRef(2, 2) = -V.coeffRef(2, 2);
			}

			cam_pts.at(i)->R = (eigen2cv(V * U.transpose()));
			cam_pts.at(i)->t = cv::Mat(avg_r(output.at(i))) - cam_pts.at(i)->R * avg_r(output.at(0));
		}
	};

private:

	std::vector<cv::Vec3d> ellipse_coords(const std::vector<cv::RotatedRect>& input, int i) const{

		std::vector<cv::Vec4f> params;
		for(int i{}; i < input.size(); ++i)
			params.push_back(cv::Vec4f(input.at(i).center.x,input.at(i).center.y , input.at(i).size.width/2.0,input.at(i).size.height/2.0 ));

		cv::Mat cam_mtx = cam_pts.at(i)->cam_mtx;

		std::vector<cv::Vec3d> output;
		for (const auto& circle : params) {
			double xs = (circle[0] - cam_mtx.at<double>(cv::Point(2, 0)))/cam_mtx.at<double>(cv::Point(0,0));
			double ys = (circle[1] - cam_mtx.at<double>(cv::Point(2, 1)))/cam_mtx.at<double>(cv::Point(1,1));

			double theta = -atan(-sqrt(xs * xs + ys * ys));
			double area = M_PI * circle[2] * circle[3] / (cam_mtx.at<double>(cv::Point(0, 0)) * cam_mtx.at<double>(cv::Point(1, 1)));
			double zs = r_sphere * sqrt(M_PI / (area * cos(theta)));

			output.push_back(cv::Vec3d(xs*zs, ys*zs, zs));
		}

		return output;
	}

	std::vector<cv::Vec3d> sphere_coords(const std::vector<cv::Vec3f>& input, int i){
		
		cv::Mat cam_mtx = cam_pts.at(i)->cam_mtx;

		std::vector<cv::Vec3d> output;
		for (const auto& circle : input) {
			double xs = (circle[0] - cam_mtx.at<double>(cv::Point(2, 0)))/cam_mtx.at<double>(cv::Point(0,0));
			double ys = (circle[1] - cam_mtx.at<double>(cv::Point(2, 1)))/cam_mtx.at<double>(cv::Point(1,1));

			double theta = -atan(-sqrt(xs * xs + ys * ys));
			double area = M_PI * circle[2] * circle[2] / (cam_mtx.at<double>(cv::Point(0, 0)) * cam_mtx.at<double>(cv::Point(1, 1)));
			double zs = r_sphere * sqrt(M_PI / (area * cos(theta)));

			output.push_back(cv::Vec3d(xs*zs, ys*zs, zs));
		}

		return output;
	};

	cv::Vec3d avg_r(const std::vector<cv::Vec3d> input) {
		cv::Vec3d sum(0.0, 0.0, 0.0);
		for (const auto vec: input)
			sum = sum + vec;
		return sum / (double)input.size();
	}
};