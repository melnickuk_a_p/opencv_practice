#pragma once

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>

inline cv::Mat method_triangulate(cv::InputArray pointsL, cv::InputArray pointsR, const cv::Mat& R, const cv::Mat& t, const cv::Mat& camL_mat, const cv::Mat& camR_mat) {

	cv::Mat_<double> P1 = cv::Mat::eye(3, 4, CV_32F);
	P1 = camL_mat * P1;

	cv::Mat_<double> P2;
	cv::hconcat(R, t, P2);
	P2 = camR_mat * P2;

	cv::Mat out;
	cv::triangulatePoints(P1, P2, pointsL, pointsR, out);
	out = out.t();

	for (int i = 0; i < out.rows; ++i)
		for (int j = 0; j < out.cols; ++j)
			out.at<float>(cv::Point(j, i)) /= out.at<float>(cv::Point(3, i));

	return out;
}

inline cv::Mat method_disparity(cv::Mat pointsL, cv::Mat pointsR, cv::InputArray Q) {

	cv::Mat output;

	std::vector<cv::Point3f> input;
	for (int i{}; i < pointsL.rows; ++i)
		for (int j{}; j < pointsL.cols; ++j)
			input.push_back(cv::Point3f(pointsL.at<cv::Point2f>(cv::Point(i, j)).x, pointsL.at<cv::Point2f>(cv::Point(i, j)).y, std::abs(pointsL.at<cv::Point2f>(cv::Point(i, j)).x - pointsR.at<cv::Point2f>(cv::Point(i, j)).x)));

	cv::perspectiveTransform(input, output, Q);

	return output.t();
};

cv::Vec3f rotationMatrixToEulerAngles(const cv::Mat& R)
{

	float sy = sqrt(R.at<double>(0, 0) * R.at<double>(0, 0) + R.at<double>(1, 0) * R.at<double>(1, 0));

	bool singular = sy < 1e-6; // If

	float x, y, z;
	if (!singular)
	{
		x = atan2(R.at<double>(2, 1), R.at<double>(2, 2));
		y = atan2(-R.at<double>(2, 0), sy);
		z = atan2(R.at<double>(1, 0), R.at<double>(0, 0));
	}
	else
	{
		x = atan2(-R.at<double>(1, 2), R.at<double>(1, 1));
		y = atan2(-R.at<double>(2, 0), sy);
		z = 0;
	}
	return cv::Vec3f(x, y, z);

}